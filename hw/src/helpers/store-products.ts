import { Product } from '../models/product';
import productsData from '../assets/product.json';

export function getProducts(): Product[] {
  return productsData;
}

export function getProductsAsync(): Promise<Product[]> {
  return Promise.resolve(getProducts());
}