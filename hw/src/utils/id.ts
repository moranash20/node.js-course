const { v4: uuidv4 } = require('uuid');

export function generateId(): string {
  return uuidv4();
}