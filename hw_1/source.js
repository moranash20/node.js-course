const events = require('events');

const NOTIFY_INTERVAL = 1000;

const event = new events.EventEmitter();
const randomNum = ()=> Math.floor(Math.random() * 100);

function init(){
    setInterval(()=> {
        event.emit('randomNumbersEvent', randomNum(), randomNum());
    }, NOTIFY_INTERVAL);
}

module.exports = {
    event,
    init
}
